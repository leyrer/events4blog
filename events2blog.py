from ics import Calendar, Event
import requests
import datetime
from datetime import date
import textwrap
import sys
import re

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

user = sys.argv[1]
pwd  = sys.argv[2]
url  = sys.argv[3]
year = sys.argv[4]

# Parse the URL
r = requests.get(url, auth=(user, pwd))
# print( "HTTP Response Encoding: " + r.encoding)

cal = Calendar(r.text)
events = cal.timeline

with open('/home/leyrer/leyrer-default-sync/Priv-Homepage/Publizieren/downloads/events/' + year + 'nerdevents.ics', 'w') as f:
    f.writelines(cal.serialize_iter())

print("Nerd Events " + year + " as of " + date.today().isoformat() + "\nmeta-Author: <a rel=\"author\" href=\"/static/about-me.html\">Martin Leyrer</a>\nTags: events, nerd, camping, ccc\n\n")
print("<p>Events marked with three question marks (\"???\") are assumed, not confirmed dates!</p>\n")
print("<p>Follow <a href=\"https://social.bau-ha.us/@ccc_regio\" title=\"(Re-)Toots von den dezentralen, lokalen Gruppen die sich der intergalaktischen Hacker*Innen Gemeinschaft des Chaos Computer Club zugehörig fühlen.\">@ccc_regio@social.bau-ha.us</a> for more infos on CfPs and local events.</p>\n")

print("<a href=\"/downloads/events/" + year + "nerdevents.ics\" alt=\"ICAL File for all events\">ICS File</a>\n\n")

# Sven Guckes Style
print("<h3><a href=\"http://www.guckes.net/events/\" title\"Sven Guckes - Events and Work in Progress\">Sven Guckes</a> Style</h3>")
print("<pre>")
lspace = 2
ldate = 16 # 16 char. for max. date diff plus 2 blanks as seperator
lname = 28
lurl  = (80-ldate-lname-(2 * lspace))

with open('my.ics', 'w') as f:
    f.writelines(cal.serialize_iter())

maxname = 0
for e in events:
    if e.name:
        l = len(e.name)
        if l > maxname:
            maxname = l

for e in events:
    if(e.end.date().year > int(year) or e.begin.date().year < int(year)):
        continue
    #print("===============================================")
    #print(vars(e))
    # Adjust enddate to ical calendar idiosyncrasy
    if e.end.date() != e.begin.date():
        e.end = e.end.shift(days=-1)

    # For <start>/<end> expressions, if any elements are missing from the end
    # value, they are assumed to be the same as for the start value including 
    # the time zone. This feature of the standard allows for concise 
    # representations of time intervals.
    if( e.end.date().month > e.begin.date().month) :
        datum = "{}/{}".format(e.begin.date().isoformat(), e.end.strftime("%m-%d") )
    elif( e.end.date() == e.begin.date()) :
        datum = "{}".format(e.begin.date().isoformat())
    else:
        datum = "{}/{}".format(e.begin.date().isoformat(), e.end.strftime("%d") )

    pdate = datum.ljust(ldate, " ")
    #sys.stderr.write(pdate.ljust(ldate+lspace, " "))
    pname = "<a href=\"" + e.description + "\">" + e.name + "</a>"
    tloc  = ",".join(e.location.split(",")[-2:])
    ploc  = re.sub("^[ 0-9]+","",tloc)

    diff = (maxname-len(e.name)) + lspace
    print(pdate.ljust(ldate+lspace, " "), end = '')
    print(pname, end = '')
    print("".ljust(diff, " "), end = '')
    print(ploc, end = '')
    print()
print("</pre>")

# max. 80 characters
print("\n<h3>80 Characters are Enough for Everybody Style</h3>")
print("<pre>")
lspace = 1
ldate = 16 # 16 char. for max. date diff plus 2 blanks as seperator
lname = (80-ldate-lspace)
lnameloc = (80-ldate-(2*lspace))
lloc  = (80-ldate-lname-(2*lspace))
lurl  = (80-ldate-lspace)

for e in events:
    if(e.end.date().year > int(year) or e.begin.date().year < int(year)):
        continue
    #print("===============================================")
    #print(vars(e))
    # Adjust edndate to NextCloud calendar idiosyncrasy
    # End date already adjusted by first run.
    #e.end = e.end.shift(days=-1)

    # For <start>/<end> expressions, if any elements are missing from the end
    # value, they are assumed to be the same as for the start value including 
    # the time zone. This feature of the standard allows for concise 
    # representations of time intervals.
    if( e.end.date().month > e.begin.date().month) :
        datum = "{}/{}".format(e.begin.date().isoformat(), e.end.strftime("%m-%d") )
    elif( e.end.date() == e.begin.date()) :
        datum = "{}".format(e.begin.date().isoformat())
    else:
        datum = "{}/{}".format(e.begin.date().isoformat(), e.end.strftime("%d") )

    pdate = datum.ljust(ldate, " ")
    pname = textwrap.wrap(e.name, width=lname)
    tloc  = ",".join(e.location.split(",")[-2:])
    tloc  = re.sub("^[ 0-9]+","",tloc)
    nameloc = e.name + ", " + tloc
    pnameloc = textwrap.wrap(nameloc, width=lnameloc, 
            initial_indent="",
            subsequent_indent=""
            )
    plink = []
    if( len(e.description) <= (80-ldate-lspace) ):
        plink.append("".ljust(ldate+lspace) + e.description)
    else:
        plink = textwrap.wrap(e.description, width=lurl, 
                initial_indent="".ljust(ldate+lspace, " "),
                break_long_words=True,
                break_on_hyphens=False
                )
    prange = len(pnameloc)
    for i in range(prange):
        if(i > 0):
            print("".ljust(ldate+lspace, " "), end = '')
        else:
            print(pdate.ljust(ldate+lspace, " "), end = '')
        if(i >= len(pnameloc)):
            print("".ljust(lnameloc, " "), end = '')
        else:
            print(pnameloc[i].ljust(lnameloc, " "), end = '')
        print()

    for i in range(len(plink)):
        if(i >= len(plink)):
            print("".ljust(lurl, " "), end = '')
        else:
            print(plink[i].ljust(lurl, " "), end = '')
        print()
print("</pre>")

print("\n<h3>HTML List</h3>")
print("<dl>")
for e in events:
    if(e.end.date().year > int(year) or e.begin.date().year < int(year)):
        continue
    # For <start>/<end> expressions, if any elements are missing from the end
    # value, they are assumed to be the same as for the start value including 
    # the time zone. This feature of the standard allows for concise 
    # representations of time intervals.
    # End date already adjusted by first run.
    # e.end = e.end.shift(days=-1)
    if( e.end.date().month > e.begin.date().month) :
        datum = "{}/{}".format(e.begin.date().isoformat(), e.end.strftime("%m-%d") )
    elif( e.end.date() == e.begin) :
        datum = "{}".format(e.begin.date().isoformat())
    else:
        datum = "{}/{}".format(e.begin.date().isoformat(), e.end.strftime("%d") )

    print("<dt>{} &#x2010; {}</dt>\n\t<dd>{}<br/>\n<a href=\"{}\">{}</a></dd>".format(datum, e.name, e.location, e.description, e.description) )
print("</dl>")


print("\n<h3>HTML Table</h3>")
print("<table><thead><th>Date</th><th>Name</th><th>Location</th></thead>")
print("<tbody>")
for e in events:
    if(e.end.date().year > int(year) or e.begin.date().year < int(year)):
        continue
    # For <start>/<end> expressions, if any elements are missing from the end
    # value, they are assumed to be the same as for the start value including 
    # the time zone. This feature of the standard allows for concise 
    # representations of time intervals.
    # End date already adjusted by first run.
    # e.end = e.end.shift(days=-1)
    if( e.end.date().month > e.begin.date().month) :
        datum = "{}/{}".format(e.begin.date().isoformat(), e.end.strftime("%m-%d") )
    elif( e.end.date() == e.begin.date()) :
        datum = "{}".format(e.begin.date().isoformat())
    else:
        datum = "{}/{}".format(e.begin.date().isoformat(), e.end.strftime("%d") )
    print("<tr><td>{}</td><td style=\"padding: 15px;\"><a href=\"{}\">{}</a></td><td>{}</td></tr>".format(datum, e.description, e.name, e.location) )
print("</tbody></table>")


count = 0
for e in events:
    if(e.end.date().year > int(year) or e.begin.date().year < int(year)):
        continue
    count = count + 1
    if( e.end.date().month > e.begin.date().month) :
        datum = "{}/{}".format(e.begin.date().isoformat(), e.end.strftime("%m-%d") )
    elif( e.end.date() == e.begin.date()) :
        datum = "{}".format(e.begin.date().isoformat())
    else:
        datum = "{}/{}".format(e.begin.date().isoformat(), e.end.strftime("%d") )

sys.stderr.write("Count: " + str(count) )
